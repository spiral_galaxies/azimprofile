# azimProfile

A script to build an image with azimuthally averaged profile

positional arguments:
    
	image              Name of image file

optional arguments:

    -h, --help           show this help message and exit

    --model MODEL        Name of output model. Default: do not save a model
 
    --azim_tab AZIM_TAB  Name of output model. Default azim_model.txt

    --mask MASK          Mask image (zero pixels are good).
  
    --xcen XCEN          x-coordinate of the object center (image center by default).
						
    --ycen YCEN          y-coordinate of the object center (image center by default).
	
	--ell ELL            Ellipticity. Default is ell=0.0
	
	--posang POSANG      Position angle of the ellipse in degrees. Up=0,
                         Left=90. Default is posang=0.0
						 
	--sma-max SMA_MAX    Maximal major axis of ellipse. Default: fitted to the image size.
	
	
    --step STEP          Linear step in semi-major axis length between successive ellipses.
  
	--diff DIFF          Name of output for difference (image -- model).
	                     Default: do not save such file.

	--rel-diff REL_DIFF  Name of output for relative difference '(image -- model)/image'.
	                     Default: do not save.
